import falcon
import json
import re
from bson.json_util import dumps
from collections import defaultdict

# 除外するログ項目
CUCKOO_PROJECTION = {
    "_id": False,
    "logname": False,
}
        
class CuckooDataset(object):
    def __init__(self, collection):
        self._collection = collection

    def on_get(self, req, resp):
        lognames = { "lognames": self._collection.distinct("logname") }
        resp.body = json.dumps(lognames, ensure_ascii=False)
        resp.status = falcon.HTTP_200

class CuckooLog(object):
    def __init__(self, collection):
        self._collection = collection
        self._nlog = {}

    def on_get(self, req, resp, logname):
        # ログの数は変わらないのでキャッシュしておく
        if self._nlog.get(logname, None) is None:
            self._nlog[logname] = self._collection.count_documents({"logname": logname})

        start = int(req.params.get("start", 0))
        count = int(req.params.get("count", self._nlog[logname]))

        logs = self._collection.find({"logname": logname}, CUCKOO_PROJECTION).skip(start).limit(count)
        resp.body = dumps(logs, ensure_ascii=False)
        resp.status = falcon.HTTP_200

class CuckooItem(object):
    def __init__(self, collection):
        self._collection = collection
        self._nlog = {}

    def on_get(self, req, resp, logname, pid):
        logs = self._collection.find_one({ "logname": logname}, CUCKOO_PROJECTION)

        #pid = pid を取得
        try:
            if isinstance(logs["behavior"]["processes"], dict):
                proc = logs["behavior"]["processes"]
                if proc["pid"] == pid:
                    log2 = proc
                else:
                    log2 = {} 
            elif isinstance(logs["behavior"]["processes"], list):
                for proc in logs["behavior"]["processes"]:
                    if proc["pid"] == pid:
                        log2 = proc
                        break
                    else:
                        log2 = {}
            else:
                log2 = {}
        except Exception as e:
            log2 = {"error": str(e)}
            
        resp.body = json.dumps(log2,ensure_ascii=False)
        resp.status = falcon.HTTP_200

