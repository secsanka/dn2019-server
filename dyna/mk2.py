import falcon
import json
import re
from bson.json_util import dumps
from collections import defaultdict

# 除外するログ項目
MK2_PROJECTION = {
    "_id": False,
    "logname": False,
}

type_converter = defaultdict(lambda: str)
numeric_type_keys = ["dst", "sn", "lv", "alert", "rs", "trs",
                     "confVer", "edition", "modified", "prevConfVer", "prevEdition",
                     "prevModified", "totalMB", "freeMB", "sessionID", "psID",
                     "zoneID", "packed", "impKrnlCnt", "size", "hide", "read",
                     "write", "pe", "valNum", "valOldNum", "page", "abrt",
                     "srcPort", "dstPort", "port", "recv", "send"]

for nk in numeric_type_keys:
    type_converter[nk] = int
        
class MK2Dataset(object):
    def __init__(self, collection):
        self._collection = collection

    def on_get(self, req, resp):
        lognames = { "lognames": self._collection.distinct("logname") }
        resp.body = json.dumps(lognames, ensure_ascii=False)
        resp.status = falcon.HTTP_200

class MK2Log(object):
    def __init__(self, collection):
        self._collection = collection
        self._nlog = {}

    def on_get(self, req, resp, logname):
        # ログの数は変わらないのでキャッシュしておく
        if self._nlog.get(logname, None) is None:
            self._nlog[logname] = self._collection.count_documents({"logname": logname})

        if req.path.endswith("count"):
            resp.body = json.dumps({"count": self._nlog[logname]}, ensure_ascii=False)
            resp.status = falcon.HTTP_200
        else:
            start = int(req.params.get("start", 0))
            count = int(req.params.get("count", self._nlog[logname]))

            logs = self._collection.find({"logname": logname}, MK2_PROJECTION).skip(start).limit(count)
            resp.body = dumps(logs, ensure_ascii=False)
            resp.status = falcon.HTTP_200

class MK2LogSearcher(object):
    def __init__(self, collection):
        self._collection = collection
        self._nlog = {}
        self._operator = {
            "eq": "$eq",
            "ne": "$ne",
            "gt": "$gt",
            "lt": "$lt",
            "gte": "$gte",
            "lte": "$lte",
            "has": "$regex",
            "and": "$and",
            "or": "$or",
        }

    # 検索用のフィルターを生成
    def filt_builder(self, json_query):
        filt = []
        if not 'condition' in json_query:
            return {}

        for key in json_query:
            if key == 'condition':
                condition = self._operator[json_query[key]]
            else:
                query, params = json_query[key]["query"], json_query[key]["param"]
                if query != "has":
                    f = [{key: {self._operator[query]: type_converter[key](param)}} for param in params]
                else:
                    f = [{key: {self._operator[query]: param}} for param in params]
                filt.append({'$or': f})

        return {condition: filt}

    def on_post(self, req, resp, logname):
        if self._nlog.get(logname, None) is None:
            self._nlog[logname] = self._collection.count_documents({"logname": logname})

        query = req.stream.read().decode('utf-8')
        if query is not "":
            json_query = json.loads(query)
        else:
            json_query = {} 

        filt = self.filt_builder(json_query)
        filt["logname"] = logname

        if req.path.endswith("count"):
            log_num = self._collection.find(filt).count()
            resp.body = json.dumps({"count": log_num}, ensure_ascii=False)
            resp.status = falcon.HTTP_200
        else:
            start = int(req.params.get("start",0))
            count = int(req.params.get("count", self._nlog[logname]))

            logs = self._collection.find(filt, MK2_PROJECTION).skip(start).limit(count)
            resp.body = dumps(logs, ensure_ascii=False)
            resp.status = falcon.HTTP_200
    
class MK2Item(object):
    def __init__(self, collection):
        self._collection = collection
        self._nlog = {}

    def on_get(self, req, resp, logname, sn):
        logs = self._collection.find({"logname": logname, "sn": sn}, MK2_PROJECTION)
        resp.body = dumps(logs, ensure_ascii=False)
        resp.status = falcon.HTTP_200
