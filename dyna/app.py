import json
import falcon
from pymongo import MongoClient
from .mk2 import MK2Dataset, MK2Log, MK2LogSearcher, MK2Item
from .cuckoo import CuckooDataset, CuckooLog, CuckooItem

class Index(object):
    def on_get(self, req, resp):
        doc = { "status": "ok" }

        resp.body = json.dumps(doc, ensure_ascii=False)
        resp.status = falcon.HTTP_200

class CORSMiddleware:
    def process_request(self, req, resp):
        resp.set_header("Access-Control-Allow-Origin", "*")
        resp.set_header("Access-Control-Allow-Headers", "Content-Type")

def create_app(collection,collection_cuckoo):
    api = falcon.API(middleware=[CORSMiddleware()])

    # Routing
    mk2item = MK2Item(collection)
    mk2log = MK2Log(collection)
    mk2log_searcher = MK2LogSearcher(collection)
    mk2dataset = MK2Dataset(collection)

    cuckooitem = CuckooItem(collection_cuckoo)
    cuckoolog = CuckooLog(collection_cuckoo)
    cuckoodataset = CuckooDataset(collection_cuckoo)

    api.add_route("/", Index())

    api.add_route("/mk2", mk2dataset)

    api.add_route("/mk2/{logname}", mk2log)
    api.add_route("/mk2/{logname}/count", mk2log)

    api.add_route("/mk2/{logname}/search", mk2log_searcher)
    api.add_route("/mk2/{logname}/search/count", mk2log_searcher)

    api.add_route("/mk2/{logname}/{sn:int}", mk2item)

    api.add_route("/cuckoo", cuckoodataset)
    api.add_route("/cuckoo/{logname}", cuckoolog)
    api.add_route("/cuckoo/{logname}/{pid:int}", cuckooitem)

    return api

def get_app():
    client = MongoClient("mongodb://localhost:27017/")
    db = client["dyna_mk2"]
    collection = db["dataset_mk2"]
    db_cuckoo = client["dyna_cuckoo"]
    collection_cuckoo = db_cuckoo["dataset_cuckoo"]
    return create_app(collection,collection_cuckoo)
