# Dynamic Navigation Server Side
## 概要
Solitoin Datasetの解析ツール

## インストール
Ubuntu上での動作を想定している．
```
$ sudo apt install python3 python3-pip mongodb
```

## 使い方
### 準備
MK2Logをjson形式に変換しておく必要がある．DatasetのToolsにあるmk2log.pyを利用すること．

```
$ python3 PATH/TO/mk2log.py json/[logname] -o json/[logname].json
$ rm -f json/[logname]
```

次に，Pythonのライブラリをインストールする．素のPythonを使う方法とPythonの仮想環境を使う方法がある．

####  素のPythonを使う場合
素のPythonを使う場合は以下を実行する．
```
$ pip3 install -r requirements.txt
```

####  Pythonの仮想環境機能を使う場合

Pythonの仮想環境を利用する場合は以下を実行する．
```
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ pip3 install -r requirements.txt
```

今後，仮想環境に入るには以下を実行する．
```
$ source venv/bin/activate
(venv) $
```

仮想環境から抜けるには以下を実行する．
```
(venv) $ deactivate
$
```

仮想環境を削除する場合は以下を実行する．
```
(venv) $ deactivate
$ rm -rf venv
```

### MongoDBにデータセットを保存
json形式のDatasetをMongoDBに登録するスクリプト(tools/setupdb.py)を利用する．

1. jsonディレクトリを作成
2. mk2log.pyで変換されたjson形式のファイルをjson/に配置
3. `python3 tools/setupdb.py`を実行

### ツールの起動
```
$ ./run.sh
```
以下のようにして、ポート番号やワーカー数を設定できる
```
$ WORKERS=4 PORT=9000 ./run.sh
```

### ベンチマーク
各URLにアクセスした際にかかった時間(秒単位)と受け取ったレスポンスのサイズ(バイト単位)が分かる．
```
$ python3 tools/bench.py | jq .
```

### テスト
unittestをを利用する．
```
$ python3 -m unittest -v
```

