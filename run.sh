#!/bin/bash

if [[ ! -v PORT ]]; then
  PORT=8000
fi

if [[ ! -v WORKERS ]]; then
  WORKERS=2
fi

gunicorn \
  --workers $WORKERS \
  --bind localhost:$PORT \
  --reload \
  'dyna.app:get_app()'
