from pymongo import MongoClient
from os import environ
import os
import json

MONGOURI = "mongodb://localhost:27017/"
DBNAME = "dyna_mk2"
COLLECTION = "dataset_mk2"
DBNAME_CUCKOO = "dyna_cuckoo"
COLLECTION_CUCKOO = "dataset_cuckoo"

# DB Objects
client = MongoClient(MONGOURI)
db = client[DBNAME]
db_cuckoo = client[DBNAME_CUCKOO]

# Create collection for Soliton Dataset
collection = db[COLLECTION]
collection_cuckoo = db_cuckoo[COLLECTION_CUCKOO]

# Hacky things
numeric_type_keys = set(["dst", "sn", "lv", "alert", "rs", "trs",
    "confVer", "edition", "modified", "prevConfVer", "prevEdition",
    "prevModified", "totalMB", "freeMB", "sessionID", "psID",
    "zoneID", "packed", "impKrnlCnt", "size", "hide", "read",
    "write", "pe", "valNum", "valOldNum", "page", "abrt",
    "srcPort", "dstPort", "port", "recv", "send"])

def initialize():
    print("Initialize DB")
    client.drop_database(db)
    client.drop_database(db_cuckoo)

def setupdb():
    print("Registering JSONs to DB from `json/`")

    for fname in os.listdir("json"):
        base, ext = os.path.splitext(fname)
        with open(os.path.join("json", fname), "r") as f:
            json_data = json.load(f)
            #cuckoo
            if "cuckoo" in fname:
                json_data['logname'] = base
                collection_cuckoo.insert_one(json_data)
            #mk2
            else:    
                for j in json_data:
                    j['logname'] = base
                for log in json_data:
                    for k, v in log.items():
                        if k in numeric_type_keys:
                            log[k] = int(log[k])
                collection.insert_many(json_data)

if __name__ == "__main__":
    if os.path.exists("json"):
        initialize()
        setupdb()
    else:
        print("`json/` not found.")
