import os
import urllib.request
import json
import time
from pprint import pprint

def get(url):
    req = urllib.request.Request(url)
    with urllib.request.urlopen(req) as res:
        body = res.read()
        return json.loads(body), len(body)

def bench(url, fn=get, ntrial=5):
    result = []
    prev_size = None
    for _ in range(ntrial):
        start_time = time.time()
        _, size = fn(url)
        if prev_size is not None and size != prev_size:
            raise ValueError("same url got different size of data")
        prev_size = size
        result.append(time.time() - start_time)
    return {"url": url, "time": sum(result) / len(result), "size": prev_size}

def main(host, port):
    urlbase = f"http://{host}:{port}"
    result = []

    result.append(bench(urlbase + "/"))
    result.append(bench(urlbase + "/mk2"))

    lognames, _ = get(urlbase + "/mk2")
    for logname in lognames["lognames"]:
        result.append(bench(urlbase + f"/mk2/{logname}"))
        result.append(bench(urlbase + f"/mk2/{logname}/count"))
        result.append(bench(urlbase + f"/mk2/{logname}/0"))

    print(json.dumps(result))

if __name__ == "__main__":
    host = os.environ.get("HOST", "localhost")
    port = os.environ.get("PORT", 8000)
    main(host, port)
